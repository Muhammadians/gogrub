//
//  HomeCell.swift
//  GoGrub
//
//  Created by apple on 10/17/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

class HomeCell: UITableViewCell {

    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var chefName: UILabel!
    @IBOutlet weak var productAvailibility: UILabel!
    @IBOutlet weak var servingLeft: UILabel!
    @IBOutlet weak var servingSize: UILabel!
    
}
