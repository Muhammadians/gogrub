//
//  HomeViewController.swift
//  GoGrub
//
//  Created by apple on 10/17/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    
    //Mark :- Variables
    var isSideViewOpen: Bool = false
    //Mark :- Arrays
    
    //Mark :- outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var popupTopConstraints: NSLayoutConstraint!
    @IBOutlet weak var location: UIButton!
    @IBOutlet weak var distance: UIButton!
    @IBOutlet weak var popupShowBtn: CustomizeButton!
    @IBOutlet weak var SellingButton: UIButton!
    @IBOutlet weak var popupChefView: UIView!
    @IBOutlet weak var sideMenu: UIView!
    @IBOutlet weak var sideMenuWidth: NSLayoutConstraint!
    @IBOutlet weak var sideMenuleadingConstraint: NSLayoutConstraint!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let chartImg    = #imageLiteral(resourceName: "shopping")
        let filterImg  = #imageLiteral(resourceName: "ic_filter")
        
        let chart   = UIBarButtonItem(image: chartImg,  style: .plain, target: self, action: #selector(HomeViewController.chart))
        let filter = UIBarButtonItem(image: filterImg,  style: .plain, target: self, action: #selector(HomeViewController.filter))
        
        let menu   = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_menu"),  style: .plain, target: self, action: #selector(HomeViewController.menu))
        let notification = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_notification_black"),  style: .plain, target: self, action: #selector(HomeViewController.notifi))
       
        navigationItem.leftBarButtonItems = [menu, notification]
        navigationItem.rightBarButtonItems = [filter, chart]
       
        navigationItem.titleView = setcenterImg()
        
        
        isSideViewOpen = false
        popupChefView.isHidden = false
        SellingButton.semanticContentAttribute = UIApplication.shared
            .userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceRightToLeft
        btnAnimation()
    }
    
    @objc func filter(){
        openFilter()
    }
    
    @objc func chart(){
        
    }
    @objc func notifi(){
        
    }
    
    @objc func menu(){
        if !isSideViewOpen{
            isSideViewOpen = true
            popupShowBtn.isHidden = true
            UIView.animate(withDuration: 1, animations: {
                self.sideMenuleadingConstraint.constant = 0
                self.view.layoutIfNeeded()
            })
        } else {
            UIView.animate(withDuration: 1, animations: {
                self.isSideViewOpen = false
                self.sideMenuleadingConstraint.constant = -275
                self.view.layoutIfNeeded()
            })
            self.popupShowBtn.isHidden = false
        }
    }
    
    @IBAction func tappedBecomeChef(_ sender: Any) {
        performSegue(withIdentifier: "becomeChef", sender: self)
    }
    
    
    @IBAction func popupBtn(_ sender: Any)
    {
       performSegue(withIdentifier: "becomeChef", sender: self)
    }
    @IBAction func btn_Dismiss(_ sender: Any)
    {
         navigationController?.setNavigationBarHidden(false, animated: true)
        popupView.isHidden = true
    }
    @IBAction func btn_DismissChef(_ sender: Any)
    {
        
        popupChefView.isHidden = true
    }
    @IBAction func tappedDistance(_ sender: Any) {
        location.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        distance.backgroundColor = #colorLiteral(red: 0.3482688665, green: 0.7006930709, blue: 0.001047337195, alpha: 1)
        distance.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: UIControl.State.normal)
        location.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: UIControl.State.normal)
    }
    @IBAction func menuButtonClicked(_ sender: Any)
    {
       
    }
    @IBAction func tappedLocation(_ sender: Any) {
        
        distance.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        location.backgroundColor = #colorLiteral(red: 0.3482688665, green: 0.7006930709, blue: 0.001047337195, alpha: 1)
        location.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: UIControl.State.normal)
        distance.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: UIControl.State.normal)
        
    }
    
    func btnAnimation()
    {
        UIView.animate(withDuration: 1, animations: {
            self.popupShowBtn.frame.origin.x -= 76
        }, completion: nil)
    }
    
    @IBAction func tappedOrderFoodBtn(_ sender: Any) {
        performSegue(withIdentifier: "order", sender: self)
    }
    
    @IBAction func tappedProfileBtn(_ sender: Any) {
        performSegue(withIdentifier: "profile", sender: self)
    }
    
    @IBAction func tappedSettingbtn(_ sender: Any) {
        performSegue(withIdentifier: "setting", sender: self)
    }
    
    @IBAction func tappedOrderHistory(_ sender: Any) {
        performSegue(withIdentifier: "orderHistory", sender: self)
    }
    
    @IBAction func tappedClearBtn(_ sender: Any) {
         navigationController?.setNavigationBarHidden(false, animated: true)
         popupView.isHidden = true
    }
    
    
    @IBAction func tappedCartBtn(_ sender: Any) {
        
    }
    
    @IBAction func tappedOkBtn(_ sender: Any) {
        
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! HomeCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "product", sender: self)
    }
    
    func openFilter()
    {
        if (popupView.isHidden == true)
        {
            navigationController?.setNavigationBarHidden(true, animated: true)
            self.popupView.isHidden = false
            UIView.animate(withDuration: 1, animations: {
                self.popupView.transform = .identity
            }, completion: { (true) in
                //self.popupTopConstraints.constant = -350
            })
        }
    }
    
    
}
