//
//  CodeVerificationViewController.swift
//  GoGrub
//
//  Created by apple on 10/17/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

class CodeVerificationViewController: UIViewController {

    //Mark :- Variables
    
    //Mark :- Arrays
    
    //Mark :- outlets
    @IBOutlet weak var first: UITextField!
    @IBOutlet weak var second: UITextField!
    @IBOutlet weak var third: UITextField!
    @IBOutlet weak var fourth: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

       self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(image: #imageLiteral(resourceName: "go-back-left-arrow (1)"), style: .plain, target: self, action: #selector(back))
        
        navigationItem.titleView = setcenterImg()
        
    }
    
    @objc func back(){self.navigationController?.popViewController(animated: true)}
    @objc func icon(){}
    @IBAction func tappedSendAgain(_ sender: Any) {
        
        performSegue(withIdentifier: "home", sender: self)
    }
}
