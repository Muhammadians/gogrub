//
//  ViewController.swift
//  GoGrub
//
//  Created by apple on 10/17/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import SVProgressHUD
import SwiftyJSON


class LoginViewController: UIViewController {

    //Mark :- Variables
    
    //Mark :- Arrays
    
    //Mark :- outlets
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var txtNumber: CustomizeTextfield!
    @IBOutlet weak var txtPassword: CustomizeTextfield!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        logoImage.image = logoImage.image?.withRenderingMode(.alwaysTemplate)
        logoImage.tintColor = UIColor.white
        
    }
    
    @IBAction func tappedContinoueBtn(_ sender: Any) {
        
        if txtNumber.text != "" && txtPassword.text != ""{
            login()
        }else{
            self.alertPopup(title: "Please Fill All Fields")
        }
        
    }
    
    @IBAction func tappedSignUpBtn(_ sender: Any) {
        performSegue(withIdentifier: "signup", sender: self)
    }
    
    @IBAction func tappedForgetBtn(_ sender: Any) {
        performSegue(withIdentifier: "forget", sender: self)
    }
    @IBAction func tappedFacebookBtn(_ sender: Any) {
        
    }
}





extension LoginViewController{
    
    func login()
    {
        loader()
        let url = URL(string: LOGIN)
        var request = URLRequest(url:url!)
        request.httpMethod = "POST"
        let parameterToSend = "mobile=" + txtNumber.text! + "&password=" + txtPassword.text! + "&device_id=" + "qwerty" + "&device_type=" + "ios" + "&fcm_token=" + "qwerty"
        request.httpBody = parameterToSend.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
            
            guard let _:Data = data else{return}
            if error != nil{self.alertPopup(title: error as! String)}
            
            let json : Any?
            do{json = try JSONSerialization.jsonObject(with: data!, options: [])}catch{return}
            
            guard let server_Response = json as? NSDictionary else{return}
            if let data_block = server_Response["message"] as? String{
                
                if  data_block == "Login Successfull."{
                    
                    DispatchQueue.main.async {
                       
                        let user = server_Response["user"] as! NSDictionary
                        id = String(user["id"] as! Int)
                        full_name = user["full_name"] as! String
                        email = user["email"] as! String
                        mobile
                            = user["mobile"] as! String
                        country_code = String(user["country_code"] as! Int)
                        descriptionss = user["description"] as! String
                       // dob = (user["dob"] as? String)!
                        success_percentage = user["success_percentage"] as! String
                        
                        UserDefaults.standard.set(server_Response["token"], forKey: "token")
                        let data = UserDefaults.standard.object(forKey: "token")
                        if let token = data as? String{
                            print(data)
                        }
                        SVProgressHUD.dismiss()
                        self.performSegue(withIdentifier: "home", sender: self)
                    }
                }else{
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        self.alertPopup(title: "Login Failed")
                    }
                }
            }
        }
        task.resume()
    }
    
}



