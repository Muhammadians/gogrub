//
//  SignupViewController.swift
//  GoGrub
//
//  Created by apple on 10/17/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import SVProgressHUD
import SwiftyJSON


class SignupViewController: UIViewController {

    //Mark :- Variables
    
    //Mark :- Arrays
    
    //Mark :- outlets
    @IBOutlet weak var signUpBtn: CustomizeButton!
    @IBOutlet weak var txtNumber: CustomizeTextfield!
    @IBOutlet weak var txtName: CustomizeTextfield!
    @IBOutlet weak var txtMail: CustomizeTextfield!
    @IBOutlet weak var txtPassword: CustomizeTextfield!
    @IBOutlet weak var txtConfirmPassword: CustomizeTextfield!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Register"
         self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(image: #imageLiteral(resourceName: "go-back-left-arrow (1)"), style: .plain, target: self, action: #selector(back))
    
        
    }
    
    @objc func back(){self.dismiss(animated: true, completion: nil)}
    
    
    @IBAction func tappedSignUpBtn(_ sender: Any) {
        performSegue(withIdentifier: "code", sender: nil)
    }
    @IBAction func tappedSignInBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func tappedFacebook(_ sender: Any) {
        
    }
}




extension SignupViewController{
    
    func signUp()
    {
        loader()
        let url = URL(string: SIGNUP)
        var request = URLRequest(url:url!)
        request.httpMethod = "POST"
        let parameterToSend = "full_name=" + txtName.text! + "&password_confirmation=" + txtConfirmPassword.text! + "&email=" + txtMail.text! + "&password=" + txtPassword.text! + "&mobile=" + txtNumber.text!
        request.httpBody = parameterToSend.data(using: String.Encoding.utf8)
        let task = URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
            
            guard let _:Data = data else{return}
            if error != nil{
                // alertPopup(title: error as! String)
            }
            
            let json : Any?
            
            do{
                json = try JSONSerialization.jsonObject(with: data!, options: [])
            }catch{
                return
            }
            
            guard let server_Response = json as? NSDictionary else{return}
            if let data_block = server_Response["message"] as? String{
                
                DispatchQueue.main.async {
                    
//                    let user = server_Response["user"] as! NSDictionary
//                    rider_id = String(user["id"] as! Int)
//                    rider_Name = user["full_name"] as! String
//                    rider_Email = user["email"] as! String
//                    rider_mobile
//                        = user["mobile"] as! String
//                    rider_rating = user["avg_rating"] as! String
//                    applied_as_chef = user["applied_as_chef"] as! Bool
//                    avg_reply_time = user["avg_reply_time"] as! Int
//                    success_percentage = user["success_percentage"] as! String
//                    UserDefaults.standard.set(server_Response["token"], forKey: "token")
//                    let data = UserDefaults.standard.object(forKey: "token")
//                    if let token = data as? String{
//                        print(data)
//                    }
                    
                    
                    SVProgressHUD.dismiss()
                    let alerts = UIAlertController.init(title: "You have registered successfully. Please enter 4 digit pin recieved on your mobile number!" , message: "", preferredStyle: .alert)
                    alerts.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (uiaction:UIAlertAction) in
                        self.performSegue(withIdentifier: "home", sender: self)
                    }))
                    self.present(alerts, animated: true, completion: nil)
                    
                }
            }
            
        }
        
        task.resume()
        
    }
    
}
