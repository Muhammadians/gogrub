//
//  ResetPasswordViewController.swift
//  GoGrub
//
//  Created by apple on 10/17/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

class ResetPasswordViewController: UIViewController {

    //Mark :- Variables
    
    //Mark :- Arrays
    
    //Mark :- outlets
    @IBOutlet weak var txtEmail: CustomizeTextfield!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Reset Password"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(image: #imageLiteral(resourceName: "go-back-left-arrow (1)"), style: .plain, target: self, action: #selector(back))
        
        
        
    }
    
    @objc func back(){self.dismiss(animated: true, completion: nil)}

    @IBAction func tappedSignIn(_ sender: Any) {
    }
    
    
    @IBAction func tappedFacebook(_ sender: Any) {
    }
    
    @IBAction func tappedGetPasswordBtn(_ sender: Any) {
    }
}
