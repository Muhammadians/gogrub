//
//  ProductDetailViewController.swift
//  GoGrub
//
//  Created by apple on 10/17/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

class ProductDetailViewController: UIViewController
{
    
    @IBOutlet weak var aboutChefUIView: CustomizeUIView!
    @IBOutlet weak var FoodPopupView: CustomizeUIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let UITapViewGestureReconizer = UITapGestureRecognizer(target: self, action: #selector(ProductDetailViewController.aboutChefClicked(recognizer:)))
        UITapViewGestureReconizer.numberOfTapsRequired = 1
        aboutChefUIView.isUserInteractionEnabled = true
        aboutChefUIView.addGestureRecognizer(UITapViewGestureReconizer)
    }
    
    @objc func aboutChefClicked(recognizer: UITapGestureRecognizer){
        print("UIView Tapped")
        self.performSegue(withIdentifier: "chefDetailVC", sender: self)
    }
    @IBAction func btn_Clicked(_ sender: Any)
    {
        FoodPopupView.isHidden = false
    }
    @IBAction func btn_Dismiss(_ sender: Any){
    
        FoodPopupView.isHidden = true
    }
    @IBAction func tappedAboutChef(_ sender: Any) {
        
        performSegue(withIdentifier: "detail", sender: self)
   
    }
}
