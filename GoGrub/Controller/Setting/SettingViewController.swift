//
//  SettingViewController.swift
//  GoGrub
//
//  Created by apple on 10/17/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

class SettingViewController: UIViewController {

    //Mark :- Variables
    
    //Mark :- Arrays
    
    //Mark :- outlets
    
    override func viewDidLoad() {
        super.viewDidLoad()

    
    }
    @IBAction func termClicked(_ sender: Any)
    {
        self.performSegue(withIdentifier: "segToTerm", sender: self)
    }
    @IBAction func aboutClicked(_ sender: Any)
    {
        self.performSegue(withIdentifier: "segToAbout", sender: self)
    }
    @IBAction func privacyClicked(_ sender: Any)
    {
        self.performSegue(withIdentifier: "segToPrivacy", sender: self)
    }
    @IBAction func tappedLogoutBtn(_ sender: Any) {
        let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "login") as! LoginViewController
        self.present(loginVC, animated: true, completion: nil)
    }
}
