//
//  SplashViewController.swift
//  GoGrub
//
//  Created by apple on 10/17/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {

    @IBOutlet weak var logoImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        animateSplash()
    }
    func animateSplash(){
        UIView.animate(withDuration: 2, animations: {
            self.logoImage.frame.size.width += 20
            self.logoImage.frame.size.height += 20
        }, completion: { (true) in
            self.fadeOut()
        })
    }
    func fadeOut()
    {
        UIView.animate(withDuration: 2, animations: {
            self.logoImage.frame.size.width -= 20
            self.logoImage.frame.size.height -= 20
        }, completion: { (true) in
            self.performSegue(withIdentifier: "splashToHome", sender: self)
        })
    }
}
