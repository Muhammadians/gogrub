//
//  EditOrderViewController.swift
//  GoGrub
//
//  Created by apple on 10/18/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import DropDown

class EditOrderViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var deliveryTextfield: UITextField!
    
    var DeliveryDropDown = DropDown()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        deliveryTextfield.delegate = self
        deliveryFunc()
    }
    
    func deliveryFunc(){
        let tapState = UITapGestureRecognizer(target: self, action: #selector(self.handleStateDD(_:)))
        deliveryTextfield.addGestureRecognizer(tapState)
        DeliveryDropDown.anchorView = deliveryTextfield
        DeliveryDropDown.dataSource = ["45 mins", "60 mins", "90 mins"]
    }
    @objc func handleStateDD(_ sender: UITapGestureRecognizer? = nil) {
        // handling code
        DeliveryDropDown.show()
        DeliveryDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.deliveryTextfield.text = item
        }
    }

}
