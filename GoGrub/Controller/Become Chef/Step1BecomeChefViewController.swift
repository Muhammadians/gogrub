//
//  Step1BecomeChefViewController.swift
//  GoGrub
//
//  Created by Muhammad Zunair on 28/10/2019.
//  Copyright © 2019 apple. All rights reserved.
//
import UIKit

class Step1BecomeChefViewController: UIViewController {

    //Mark :- Variables
    
    //Mark :- Arrays
    
    //Mark :- outlets
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var cameraBtn: UIButton!
    @IBOutlet weak var photoBtn: UIButton!
    @IBOutlet weak var img: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.nextBtn.layer.cornerRadius = 8.0
        
    }
    
    @IBAction func tappedPhotoBtn(_ sender: Any) {
    }
    
    @IBAction func tappedCameraBtn(_ sender: Any) {
    }
    
    @IBAction func tappedNextBtn(_ sender: Any) {
    }

}
