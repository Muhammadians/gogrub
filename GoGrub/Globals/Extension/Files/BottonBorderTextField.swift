//
//  BottonBorderTextField.swift
//  GoGrub
//
//  Created by apple on 10/17/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
@IBDesignable

class BottonBorderTextField: UITextField {

    var bottomBorder = UIView()
    override func awakeFromNib()
    {
        // Setup Bottom-Border
        self.translatesAutoresizingMaskIntoConstraints = false
        bottomBorder = UIView.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        bottomBorder.backgroundColor = UIColor(red: 0.1, green: 0.1, blue: 0.1, alpha: 1.0)// Set Border-Color
        bottomBorder.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(bottomBorder)
        
        bottomBorder.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        bottomBorder.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        bottomBorder.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        bottomBorder.heightAnchor.constraint(equalToConstant: 1).isActive = true // Set Border-Strength
    }
    
}
