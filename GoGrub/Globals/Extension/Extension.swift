//
//  Extension.swift
//  GoGrub
//
//  Created by Muhammad Zunair on 27/10/2019.
//  Copyright © 2019 apple. All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD
import SwiftyJSON

extension UIViewController
{
    
    func alertPopup(title : String)
    {
        let alerts = UIAlertController.init(title: title , message: "", preferredStyle: .alert)
        alerts.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (uiaction:UIAlertAction) in
            print("alert actions active")
        }))
        self.present(alerts, animated: true, completion: nil)
        
    }
    func hideKeyboard()
    {
        let tap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard()
    {
        view.endEditing(true)
    }
    
    func loader(){
        SVProgressHUD.show()
        let color = #colorLiteral(red: 0.9038441777, green: 0.6588760018, blue: 0.04511878639, alpha: 1)
        SVProgressHUD.setForegroundColor(color)
    }
    
    func setcenterImg()->UIImageView{
        let imageVu = UIImageView(frame: CGRect(x: -100.0, y: 0.0, width: 40.0, height: 40.0))
        imageVu.contentMode = .scaleAspectFit
        imageVu.image = UIImage(named: "cart")
        return imageVu
    }
    
    
    
}
